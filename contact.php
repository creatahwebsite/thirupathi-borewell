


<?php
require_once("mail/mail.php");
    if(isset($_POST["submit_flag1"])) {
        $b_name = $_POST['name'];
        $b_num = $_POST["phone"];
        $be_msg =  $_POST["message"];
        $subject="New Enquiry - Request Callback ";
        $admin_msg =  "<p>Dear Admin, THIRUPATHI Borewells</p>
        <p>You have received request call back enquiry. Please check the below details</p>
        <p></p>
        <p>Name: ".$b_name."</p>
        <p>Phone: ".$b_num."</p>
        <p>Message: ".$be_msg."</p>";
        
        // Form validation
        if(!empty($b_name) && !empty($b_num) && !empty($be_msg)){
            // reCAPTCHA validation
            if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
                // Google secret API
                $secretAPIkey = '6LctC5wfAAAAAI6hQg5CfSksPKsUBzj1D2bFmUZ0';
                // reCAPTCHA response verification
                $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secretAPIkey.'&response='.$_POST['g-recaptcha-response']);
                // Decode JSON data
                $response = json_decode($verifyResponse);
                    if($response->success){
                        $toMail = "thirupathiborewell1974@gmail.com";
                        //  $toMail = "mustaq.creatah@gmail.com";

                        $header = "From: " . $b_name . "<". $b_mail .">\r\n";
                        mailer($subject,$admin_msg,$toMail);
                        // if($dai){
                        //     echo "sasi";
                        // }
                        $response = array(
                            "status" => "alert-success",
                            "message" => "Your mail have been sent."
                        );
                    } else {
                        $response = array(
                            "status" => "alert-danger",
                            "message" => "Robot verification failed, please try again."
                        );
                    }       
            } else{ 
                $response = array(
                    "status" => "alert-danger",
                    "message" => "Plese check on the reCAPTCHA box."
                );
            } 
        }  else{ 
            $response = array(
                "status" => "alert-danger",
                "message" => "All the fields are required."
            );
        }
    }  
?>





<!DOCTYPE html>
<html  xml:lang="en-US" lang="en-US">

<head>

<meta charset="utf-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title>THIRUPATHI Borewells | Contact</title>
<meta name="author" content="themesflat.com">

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" type="text/css" href="style.css">

<link rel="stylesheet" type="text/css" href="assets/css/colors/color1.css" id="colors">

<link rel="shortcut icon" href="assets/icon/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="assets/icon/apple-touch-icon-158-precomposed.png">
<!--[if lt IE 9]>
        <script src="javascript/html5shiv.js"></script>
        <script src="javascript/respond.min.js"></script>
    <![endif]-->
</head>
<body class="header-fixed page no-sidebar header-style-2 topbar-style-1 menu-has-search">
<div id="wrapper" class="animsition">
<div id="page" class="clearfix">

<?php include  'header.php';?>

<div id="featured-title" class="featured-title clearfix">
<div id="featured-title-inner" class="container clearfix">
<div class="featured-title-inner-wrap">
<div id="breadcrumbs">
<div class="breadcrumbs-inner">
<div class="breadcrumb-trail">
<a href="index.php" class="trail-begin">Home</a>
<span class="sep">|</span>
<span class="trail-end">Contact</span>
</div>
</div>
</div>
<div class="featured-title-heading-wrap">
<h1 class="feautured-title-heading">
Contact Us
</h1>
</div>
</div>
</div>
</div>


<div id="main-content" class="site-main clearfix">
<div id="content-wrap">
<div id="site-content" class="site-content clearfix">
<div id="inner-content" class="inner-content-wrap">
<div class="page-content">

<div class="row-contact">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="themesflat-spacer clearfix" data-desktop="61" data-mobile="60" data-smobile="60"></div>
</div>
</div>
<div class="row">
<div class="col-md-6">
<div class="themesflat-headings style-2 clearfix">
<h2 class="heading">CONTACT US</h2>
<div class="sep has-width w80 accent-bg clearfix"></div>
<p class="sub-heading">Are you interested in finding out how THIRUPATHI Borewells Services can make your project. For more information on our services please contact us.</p>
</div>
<div class="themesflat-spacer clearfix" data-desktop="36" data-mobile="35" data-smobile="35"></div>
<div class="themesflat-contact-form style-2 clearfix">
<form  action="#" method="post">
<span class="wpcf7-form-control-wrap your-name">
<input type="text"  value=""  placeholder="Name*"  name="name" id="name" >
</span>
<span class="wpcf7-form-control-wrap your-phone">
<input type="text" tabindex="2"  value=""  placeholder="Phone" name="phone" id="phone">
</span>


<span class="wpcf7-form-control-wrap your-message">
<textarea tabindex="5" cols="10" rows="5"  placeholder="message*" name="message" id="contact-message'"></textarea>
</span>
  <div class="g-recaptcha" data-sitekey="6LctC5wfAAAAALc0FHWjKI11BWsFbE8h7YgGCpe_"></div>
   <?php if(!empty($response)) {?>
        <div class="form-group col-12 text-center">
          <div class="alert text-center <?php echo $response['status']; ?>">
            <?php echo $response['message']; ?>
          </div>
        </div>
        <?php }?>
<span class="wrap-submit">
<input type="submit" name="submit_flag1">
</span>
</form>
</div>
</div>
<div class="col-md-6">
<div class="themesflat-spacer clearfix" data-desktop="0" data-mobile="0" data-smobile="35"></div>
<div class="themesflat-headings style-2 clearfix">
<h2 class="heading">INFOMATION</h2>
<div class="sep has-width w80 accent-bg clearfix"></div>
</div>
<div class="themesflat-spacer clearfix" data-desktop="36" data-mobile="35" data-smobile="35"></div>
<div class="themesflat-tabs style-1 w168 clearfix">

<div class="tab-content-wrap clearfix">
<div class="tab-content">
<div class="item-content">
<ul>
<li class="clearfix">
<div class="inner">
<span class="fa fa-map-marker"></span>
<span class="text">54/105 Bajanai Koil Street, <br>, Chennai - 600094<br> (Near Bajani Temple) </span>
</div>
</li>
<li class="clearfix">
<div class="inner">
<span class="fa fa-phone"></span>
<span class="text">CALL US : +91 9841169978, +91 90923 72735</span>
</div>
</li>
<li class="clearfix">
<div class="inner">
<span class="fa fa-envelope"></span>
<span class="text">EMAIL : thirupathiborewell1974@gmail.com</span>
</div>
</li>
</ul>
</div>
</div>


</div>
</div>

<div class="themesflat-spacer clearfix" data-desktop="20" data-mobile="35" data-smobile="35"></div>
<!-- <div class="themesflat-map"></div> -->
<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3929.685374475073!2d78.1421586!3d9.9601134!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b00c5d69dff96f3%3A0x6f02d0e5986ecc!2sIyer%20Bungalow%20Rd%2C%20Madurai%2C%20Tamil%20Nadu!5e0!3m2!1sen!2sin!4v1641627253213!5m2!1sen!2sin" width="600" height="250" style="border:0;" allowfullscreen="" loading="lazy"></iframe> -->
</div>
</div>
<div class="row">
<div class="col-md-12">
<div class="themesflat-spacer clearfix" data-desktop="78" data-mobile="60" data-smobile="60"></div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>

<?php include  'footer.php';?>
<a id="scroll-top"></a>


<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="assets/js/jquery.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/tether.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/animsition.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/countto.js"></script>
<script src="assets/js/equalize.min.js"></script>
<script src="assets/js/jquery.isotope.min.js"></script>
<script src="assets/js/owl.carousel2.thumbs.js"></script>
<script src="assets/js/jquery.cookie.js"></script>
<script src="assets/js/gmap3.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIEU6OT3xqCksCetQeNLIPps6-AYrhq-s&amp;region=GB"></script>
<script src="assets/js/shortcodes.js"></script>
<script src="assets/js/jquery-validate.js"></script>
<script src="assets/js/main.js"></script>

<script src="includes/rev-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="includes/rev-slider/js/jquery.themepunch.revolution.min.js"></script>
<script src="assets/js/rev-slider.js"></script>

<script src="includes/rev-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.video.min.js"></script>
</body>

<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>

<script src="https://www.google.com/recaptcha/api.js"></script>
 <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
    async defer>
    
</script> 
</html>
