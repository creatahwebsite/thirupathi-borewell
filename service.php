<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->

<head>

<meta charset="utf-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title>THIRUPATHI Borewells | Service </title>
<meta name="author" content="themesflat.com">

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" type="text/css" href="style.css">

<link rel="stylesheet" type="text/css" href="assets/css/colors/color1.css" id="colors">

<link rel="shortcut icon" href="assets/icon/favicon.png">
<link rel="apple-touch-icon-precomposed" href="assets/icon/apple-touch-icon-158-precomposed.png">
<!--[if lt IE 9]>
        <script src="javascript/html5shiv.js"></script>
        <script src="javascript/respond.min.js"></script>
    <![endif]-->
</head>
<body class="header-fixed page no-sidebar header-style-2 topbar-style-1 menu-has-search">
<div id="wrapper" class="animsition">
<div id="page" class="clearfix">

<?php include  'header.php';?>

<div id="featured-title" class="featured-title clearfix">
<div id="featured-title-inner" class="container clearfix">
<div class="featured-title-inner-wrap">
<div id="breadcrumbs">
<div class="breadcrumbs-inner">
<div class="breadcrumb-trail">
<a href="index.php" class="trail-begin">Home</a>
<span class="sep">|</span>
<span class="trail-end">Services</span>
</div>
</div>
</div>
<div class="featured-title-heading-wrap">
<h1 class="feautured-title-heading">
All Services
</h1>
</div>
</div>
</div>
</div>
 




<div id="main-content" class="site-main clearfix">
<div id="content-wrap">
<div id="site-content" class="site-content clearfix">
<div id="inner-content" class="inner-content-wrap">
<div class="page-content">

<div class="row-services">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="themesflat-spacer clearfix" data-desktop="81" data-mobile="60" data-smobile="60"></div>
<div class="themesflat-carousel-box data-effect clearfix" data-gap="30" data-column="3" data-column2="2" data-column3="1" data-auto="true">
<div class="owl-carousel owl-theme">
<div class="themesflat-image-box style-2 clearfix">
<div class="image-box-item">
<div class="inner">
<div class="thumb data-effect-item">
<img src="assets/img/imagebox/01.png" alt="Image">

</div>
<div class="text-wrap">
<h5 class="heading"> PLAN APPROVAL</h5>
<!-- <p class="letter-spacing-01">We develop and understand project expectations and then manage those needs with a design [...]</p> -->
<div class="elm-readmore">
<!-- <a href="page-services-detail.html">DETAILS</a> -->
</div>
</div>
</div>
</div>
</div>

<div class="themesflat-image-box style-2 clearfix">
<div class="image-box-item">
<div class="inner">
<div class="thumb data-effect-item">
<img src="assets/img/imagebox/02.png" alt="Image">

</div>
<div class="text-wrap">
<h5 class="heading"> 3D ELEVATION DRAWING </h5>
<!-- <p class="letter-spacing-01">We work closely with architects to understand the project, and ultimately develop a targeted [...]</p> -->
<div class="elm-readmore">
<!-- <a href="page-services-detail.html">DETAILS</a> -->
</div>
</div>
</div>
</div>
</div>
<div class="themesflat-image-box style-2 clearfix">
<div class="image-box-item">
<div class="inner">
<div class="thumb data-effect-item">
<img src="assets/img/imagebox/03.png" alt="Image">

</div>
<div class="text-wrap">
<h5 class="heading"> ESTIMATE </h5>
<!-- <p class="letter-spacing-01">In the past decade alone, we have completed more than 5 million square feet of metal [...]</p> -->
<div class="elm-readmore">
<!-- <a href="page-services-detail.html">DETAILS</a> -->
</div>
</div>
</div>
</div>
</div>



</div>
</div>
<div class="themesflat-spacer clearfix" data-desktop="15" data-mobile="15" data-smobile="15"></div>
<div class="themesflat-carousel-box data-effect clearfix" data-gap="30" data-column="3" data-column2="2" data-column3="1" data-auto="true">
<div class="owl-carousel owl-theme">
<div class="themesflat-image-box style-2 clearfix">
<div class="image-box-item">
<div class="inner">
<div class="thumb data-effect-item">
<img src="assets/img/imagebox/04.png" alt="Image">




</div>
<div class="text-wrap">
<h5 class="heading">  VASTHU </h5>
<!-- <p class="letter-spacing-01">In the past decade alone, we have completed more than 5 million square feet of metal building [...]</p> -->
<div class="elm-readmore">
<!-- <a href="page-services-detail.html">DETAILS</a> -->
</div>
</div>
</div>
</div>
</div>
<div class="themesflat-image-box style-2 clearfix">
<div class="image-box-item">
<div class="inner">
<div class="thumb data-effect-item">
<img src="assets/img/imagebox/05.png" alt="Image">

 </div>
<div class="text-wrap">
<h5 class="heading"> WATER PROOFING WORKS</h5>
<!-- <p class="letter-spacing-01">Autora Construction Services provides the right resources and expertise to evaluate concepts [...]</p> -->
<div class="elm-readmore">
<!-- <a href="page-services-detail.html">DETAILS</a> -->
</div>
</div>
</div>
</div>
</div>
<div class="themesflat-image-box style-2 clearfix">
<div class="image-box-item">
<div class="inner">
<div class="thumb data-effect-item">
<img src="assets/img/imagebox/07.png" alt="Image">

 </div>
<div class="text-wrap">
<h5 class="heading"> LOAN ARRANGEMENT</h5>
<!-- <p class="letter-spacing-01">Autora Construction Services provides the right resources and expertise to evaluate concepts [...]</p> -->
<div class="elm-readmore">
<!-- <a href="page-services-detail.html">DETAILS</a> -->
</div>
</div>
</div>
</div>
</div>
<!-- <div class="themesflat-image-box style-2 clearfix">
<div class="image-box-item">
<div class="inner">
<div class="thumb data-effect-item">
<img src="assets/img/imagebox/06.png" alt="Image">
<div class="overlay-effect bg-color-accent"></div>
</div>
<div class="text-wrap">
<h5 class="heading"> ALL TYPES OF CIVIL WORKS </h5>
<p class="letter-spacing-01">We employs a talented team of industry leading professionals capable of self-performing [...]</p>
<div class="elm-readmore">
<a href="page-services-detail.html">DETAILS</a>
</div>
</div>
</div>
</div>
</div> -->




</div>
</div>
 <div class="themesflat-spacer clearfix" data-desktop="45" data-mobile="60" data-smobile="60"></div>
</div>

</div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>



<?php include  'footer.php';?>
<a id="scroll-top"></a>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="assets/js/jquery.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/tether.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/animsition.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/countto.js"></script>
<script src="assets/js/equalize.min.js"></script>
<script src="assets/js/jquery.isotope.min.js"></script>
<script src="assets/js/owl.carousel2.thumbs.js"></script>
<script src="assets/js/jquery.cookie.js"></script>
<script src="assets/js/gmap3.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIEU6OT3xqCksCetQeNLIPps6-AYrhq-s&amp;region=GB"></script>
<script src="assets/js/shortcodes.js"></script>
<script src="assets/js/main.js"></script>

<script src="includes/rev-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="includes/rev-slider/js/jquery.themepunch.revolution.min.js"></script>
<script src="assets/js/rev-slider.js"></script>

<script src="includes/rev-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.video.min.js"></script>
</body>


</html>
