
<footer id="footer" class="clearfix">
<div id="footer-widgets" class="container">
<div class="themesflat-row gutter-30">
<div class="col span_1_of_3">
<div class="widget widget_text">
<div class="textwidget">
<p>
<a href="index.php" rel="home" class="main-logo">
<img src="./assets/images/logo.jpg" >
</a>
</p>
<p class="margin-bottom-15 " style="justify-content: flex-start;">BOREWELL CLEANING SERVICES IN CHENNAI
We at THIRUPATHI Borewells are the most popular Borewell Drilling Company in Chennai and deliver affordable borewell drilling services.</p>

</div>
</div>
<div class="themesflat-spacer clearfix" data-desktop="0" data-mobile="0" data-smobile="35"></div>
</div>
<div class="col span_1_of_3">
<div class="themesflat-spacer clearfix" data-desktop="0" data-mobile="0" data-smobile="0"></div>
<div class="widget widget_lastest">
<h2 class="widget-title"><span>CONTACT-INFO</span></h2>
<div class="widget widget_text">
<div class="textwidget">
<p>


<ul>
<li>
<div class="inner">
<span class="fa fa-map-marker"></span>
<span class="text">54/105 Bajanai Koil Street,  <br> <span style="padding-left: 0rem;"> Choolaimedu,<br></span>
<span><span style="padding-left: 4rem;">Chennai - 600094 </span> <br><span style="padding-left: 4rem;"> (Near Bajani Temple)</span></span>
</div>
</li>
<li>
<div class="inner">
<span class="fa fa-phone"></span>
<span class="text"> +91 98411 69978 <br> +91 90923 72735</span>
</div>
</li>
<li class="margin-top-7">
<div class="inner">
<span class=" font-size-14 fa fa-envelope"></span>
<span class="text">thirupathiborewell1974@gmail.com</span>
</div>
</li>
</ul>
</div>
</div>

</div>
</div>
<div class="col span_1_of_3">
<div class="themesflat-spacer clearfix" data-desktop="0" data-mobile="35" data-smobile="35"></div>
<div class="widget widget_tags">
<h2 class="widget-title margin-bottom-30"><span>OUR SERVICES</span></h2>
<div class="tags-list">
<a>Borewell Services </a>
<a>Borewell Cleaning</a>

<a> Borewell Repair & Services</a>
<!-- <a>LOAN ARRANGEMENT</a>
<a>ESTIMATE</a>
<a> VASTHU </a> -->





 
  




</div>
</div>
</div>
<div class="col span_1_of_3">
<div class="themesflat-spacer clearfix" data-desktop="0" data-mobile="35" data-smobile="35"></div>
<div class="widget widget_instagram">
<h2 class="widget-title margin-bottom-30"><span>Our Gallery</span></h2>
<div class="instagram-wrap data-effect clearfix col3 g10">
<div class="instagram_badge_image has-effect-icon">

<span class="item"><img src="./assets/images/service-01.png" alt="Image"></span>
<div class="overlay-effect bg-color-2"></div>
<div class="elm-link">

</div>
</a>
</div>
<div class="instagram_badge_image has-effect-icon">

<span class="item"><img src="./assets/images/service-2.png" alt="Image"></span>
<div class="overlay-effect bg-color-2"></div>
<div class="elm-link">

</div>
</a>

</div>
<div class="instagram_badge_image has-effect-icon">

<span class="item"><img src="./assets/images/service-3.png" alt="Image"></span>
<div class="overlay-effect bg-color-2"></div>
<div class="elm-link">

</div>
</a>
</div>



</div>
</div>
</div>
</div>
</div>
</footer>

<div id="bottom" class="clearfix has-spacer">
<div id="bottom-bar-inner" class="container">
<div class="bottom-bar-inner-wrap">
<div class="bottom-bar-content">
<div id="copyright"> © <span class="text">THIRUPATHI Borewells </span>
</div>
</div>
<div class="bottom-bar-menu">
<ul class="bottom-nav">

<li class="menu-item">
<a href="index.php">HOME</a>
</li>
<li class="menu-item">
<a href="about.php">ABOUT US</a>
</li>
<li class="menu-item">
 <a href="service-detail.php">SERVICES</a>
</li>



<li class="menu-item">
<a href="contact.php">CONTACT</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>


<!-- <a href="https://wa.me/919842153050" target="_blank" class="float">
    <img src="whatsapp.png" width="65px">
</a> -->
<!-- 
         <a href="#"  target="_blank" class="float1">
    <img src="assets/img/sidebar.png" width="65px">
</a>  -->

<div class="call-buton"><a class="cc-calto-action-ripple" href="tel: +91 9841169978"><i class="fa fa-phone"></i></a>
</div>

<style>
.float {
    position: fixed;
    width: 50px;
    height: 50px;
    bottom: 45px;
    left: 25px; 
 /*  right: 15px;*/
    z-index: 100;
    color: #FFF;
    border-radius: 20px;
    text-align: center;
    box-shadow: 2px 2px 3px #9990;
}

.call-buton .cc-calto-action-ripple {
    z-index: 99999;
    position: fixed;
    bottom: 45px;
    left: 25px; 
    font-size: 22px;
    background: #08aff8;
    width: 4rem;
    height: 4rem;
    padding: 1rem;
    border-radius: 100%;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    color: #ffffff;
    -webkit-animation: cc-calto-action-ripple 0.6s linear infinite;
    animation: cc-calto-action-ripple 0.6s linear infinite;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    justify-items: center;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
    text-decoration: none;
}

</style>