<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->

<head>

<meta charset="utf-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
<title>THIRUPATHI Borewells | About</title>
<meta name="author" content="themesflat.com">

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" type="text/css" href="style.css">

<link rel="stylesheet" type="text/css" href="assets/css/colors/color1.css" id="colors">

<link rel="shortcut icon" href="assets/icon/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="assets/icon/apple-touch-icon-158-precomposed.png">
<!--[if lt IE 9]>
        <script src="javascript/html5shiv.js"></script>
        <script src="javascript/respond.min.js"></script>
    <![endif]-->
</head>
<body class="header-fixed page no-sidebar header-style-2 topbar-style-1 menu-has-search">
<div id="wrapper" class="animsition">
<div id="page" class="clearfix">

<?php include  'header.php';?>
<div id="featured-title" class="featured-title clearfix">
<div id="featured-title-inner" class="container clearfix">
<div class="featured-title-inner-wrap">
<div id="breadcrumbs">
<div class="breadcrumbs-inner">
<div class="breadcrumb-trail">
<a href="index.php" class="trail-begin">Home</a>
<span class="sep">|</span>
<span class="trail-end">About Us</span>
</div>
</div>
</div>
<div class="featured-title-heading-wrap">
<h1 class="feautured-title-heading">
About Us
</h1>
</div>
</div>
</div>
</div>


<div id="main-content" class="site-main clearfix">
<div id="content-wrap">
<div id="site-content" class="site-content clearfix">
<div id="inner-content" class="inner-content-wrap">
<div class="page-content">

<!-- <div class="row-iconbox">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="themesflat-spacer clearfix" data-desktop="60" data-mobile="60" data-smobile="60"></div>
<div class="themesflat-headings style-1 text-center clearfix">
<h2 class="heading">YOUR BEST CHOOSE</h2>
<div class="sep has-icon width-125 clearfix">

</div>
<p class="sub-heading">More than 500 projects completed in Surya Construction</p>
</div>
<div class="themesflat-spacer clearfix" data-desktop="42" data-mobile="35" data-smobile="35"></div>
</div>
</div>
<div class="row">
<div class="col-md-4">
<div class="themesflat-content-box clearfix" data-margin="0 5px 0 5px" data-mobilemargin="0 0 0 0">
<div class="themesflat-icon-box icon-top align-center has-width w95 circle light-bg accent-color style-1 clearfix">
<div class="icon-wrap">
<i class="autora-icon-quality"></i>
</div>
<div class="text-wrap">
<h5 class="heading"><a >BEST QUALITY</a></h5>
<div class="sep clearfix"></div>
<p class="sub-heading">Surya Construction Services are committed to meeting the highest quality standards without compromising our safety culture..</p>
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div class="themesflat-spacer clearfix" data-desktop="0" data-mobile="0" data-smobile="35"></div>
<div class="themesflat-content-box clearfix" data-margin="0 5px 0 5px" data-mobilemargin="0 0 0 0">
<div class="themesflat-icon-box icon-top align-center has-width w95 circle light-bg accent-color style-1 clearfix">
<div class="icon-wrap">
<i class="autora-icon-time"></i>
</div>
<div class="text-wrap">
<h5 class="heading"><a >ON TIME</a></h5>
<div class="sep clearfix"></div>
<p class="sub-heading">At our company, we respect the customer’s time and schedule and always complete the projects on timely fashion way.</p>
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div class="themesflat-spacer clearfix" data-desktop="0" data-mobile="0" data-smobile="35"></div>
 <div class="themesflat-content-box clearfix" data-margin="0 5px 0 5px" data-mobilemargin="0 0 0 0">
<div class="themesflat-icon-box icon-top align-center has-width w95 circle light-bg accent-color style-1 clearfix">
<div class="icon-wrap">
<i class="autora-icon-author"></i>
</div>
<div class="text-wrap">
<h5 class="heading"><a>EXPERIENCED</a></h5>
<div class="sep clearfix"></div>
<p class="sub-heading">As the saying goes practice makes perfect. With our years of experience you can bet on us to get the job done exactly to your specifications.</p>
</div>
</div>
</div>
</div>
</div> -->


<div class="row">
<div class="col-md-12">
<div class="themesflat-spacer clearfix" data-desktop="41" data-mobile="35" data-smobile="35"></div>
<div class="elm-button text-center">
<a href="#" class="themesflat-button bg-accent">ABOUT US</a>
</div>
<div class="themesflat-spacer clearfix" data-desktop="73" data-mobile="60" data-smobile="60"></div>
</div>
</div>
</div>
</div>


<div class="row-about">
<div class="container-fluid">
<div class="row equalize sm-equalize-auto">
<div class="col-md-6 half-background style-1">
</div>
<div class="col-md-6 bg-light-grey">
<div class="themesflat-spacer clearfix" data-desktop="64" data-mobile="60" data-smobile="60"></div>
<div class="themesflat-content-box clearfix" data-margin="0 25% 0 4.5%" data-mobilemargin="0 0 0 4.5%">
<div class="themesflat-headings style-1 clearfix">
<h2 class="heading">Welcome to THIRUPATHI Borewells</h2>
<div class="sep has-width w80 accent-bg margin-top-11 clearfix"></div>
<p class="sub-heading margin-top-30">BOREWELL CLEANING SERVICES IN CHENNAI
We at THIRUPATHI Borewells are the most popular Borewell Drilling Company in Chennai and deliver affordable borewell drilling services for a wide range of both the domestic and the industrial client base.
</p>
<p class="sub-heading margin-top-30">Being an end-end Borewell Cleaning Service Providing Company in Chennai, Thirupathi Borewells holds more than two decades of experience and delivers its expertise in Borewell Drilling, Old Borewell Cleaning, Old Borewell Repair services, Hand Borewell Drilling, Rain Water Harvesting, Soil Test, Borewell Drilling Galaxy Method performance, Power Rig Borewell Drilling Method in domestic and industrial spaces as well.</p>
<p class="sub-heading margin-top-30">Our uniqueness gets exhibited in the way we provide the borewell cleaning approach where we follow all the major three practices like power Ring Borewell Cleaning, Air Compressor Borewell Cleaning, and Hand Borewell Cleaning Service.</p>
<p class="sub-heading margin-top-30">Established in the year 1995, Tirupathi  Borewells Services in  Chennai is a top player in the category Borewell Contractors in Chennai.</p>

</div>
<div class="themesflat-spacer clearfix" data-desktop="26" data-mobile="35" data-smobile="35"></div>
<div class="content-list">
<div class="themesflat-list has-icon style-1 icon-left clearfix">
<div class="inner">
<span class="item">

</span>
</div>
</div>



</div>
<div class="themesflat-spacer clearfix" data-desktop="42" data-mobile="35" data-smobile="35"></div>

</div>
<div class="themesflat-spacer clearfix" data-desktop="75" data-mobile="60" data-smobile="60"></div>
</div>
</div>
</div>
</div>












<?php include  'footer.php';?>
<a id="scroll-top"></a>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="assets/js/jquery.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/tether.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/animsition.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/countto.js"></script>
<script src="assets/js/equalize.min.js"></script>
<script src="assets/js/jquery.isotope.min.js"></script>
<script src="assets/js/owl.carousel2.thumbs.js"></script>
<script src="assets/js/jquery.cookie.js"></script>
<script src="assets/js/gmap3.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAIEU6OT3xqCksCetQeNLIPps6-AYrhq-s&amp;region=GB"></script>
<script src="assets/js/shortcodes.js"></script>
<script src="assets/js/main.js"></script>

<script src="includes/rev-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="includes/rev-slider/js/jquery.themepunch.revolution.min.js"></script>
<script src="assets/js/rev-slider.js"></script>

<script src="includes/rev-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="includes/rev-slider/js/extensions/revolution.extension.video.min.js"></script>
</body>


</html>
